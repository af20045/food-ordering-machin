import javax.swing.*;

import static javax.swing.JOptionPane.INFORMATION_MESSAGE;

public class FoodOrderingMachineGUI {
    public static void main(String[] args) {
        JFrame frame = new JFrame("FoodOrderingMachineGUI");
        frame.setContentPane(new FoodOrderingMachineGUI().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    private JPanel root;
    private JButton checkOutButton;
    private JTextPane orderedItemsList;
    private JButton button0;
    private JButton button1;
    private JButton button2;
    private JButton button3;
    private JButton button4;
    private JButton button5;
    private JLabel TotalPrice;
    int sum = 0;


    public FoodOrderingMachineGUI() {
        String[] path = new String[6];
        path[0] = "image/sushi_tamago.png";
        path[1] = "image/sushi_tai.png";
        path[2] = "image/sushi_salmon.png";
        path[3] = "image/sushi_hotate.png";
        path[4] = "image/sushi_ebi.png";
        path[5] = "image/sushi_akami.png";

        String[] name = new String[6];
        name[0] = "Tamago";
        name[1] = "Tai";
        name[2] = "Salmon";
        name[3] = "Hotate";
        name[4] = "Ebi";
        name[5] = "Akami";

        int[] price = new int[6];
        price[0] = 100;
        price[1] = 150;
        price[2] = 120;
        price[3] = 140;
        price[4] = 110;
        price[5] = 130;

        button0.setIcon(new ImageIcon(this.getClass().getResource(path[0])));
        button1.setIcon(new ImageIcon(this.getClass().getResource(path[1])));
        button2.setIcon(new ImageIcon(this.getClass().getResource(path[2])));
        button3.setIcon(new ImageIcon(this.getClass().getResource(path[3])));
        button4.setIcon(new ImageIcon(this.getClass().getResource(path[4])));
        button5.setIcon(new ImageIcon(this.getClass().getResource(path[5])));
        button0.setText(name[0] + " " + price[0] + "yen");
        button1.setText(name[1] + " " + price[1] + "yen");
        button2.setText(name[2] + " " + price[2] + "yen");
        button3.setText(name[3] + " " + price[3] + "yen");
        button4.setText(name[4] + " " + price[4] + "yen");
        button5.setText(name[5] + " " + price[5] + "yen");

        button0.addActionListener(e -> sum = order(name[0], price[0], sum));
        button1.addActionListener(e -> sum = order(name[1], price[1], sum));
        button2.addActionListener(e -> sum = order(name[2], price[2], sum));
        button3.addActionListener(e -> sum = order(name[3], price[3], sum));
        button4.addActionListener(e -> sum = order(name[4], price[4], sum));
        button5.addActionListener(e -> sum = order(name[5], price[5], sum));
        checkOutButton.addActionListener(e -> {
            int confirmation = JOptionPane.showConfirmDialog(
                    null,
                    "Would you like to checkout?",
                    "Checkout Confirmation",
                    JOptionPane.YES_NO_OPTION);
            if (confirmation == 0) {
                JOptionPane.showMessageDialog(
                        null,
                        "Thank you. The price is " + sum + " yen.",
                        "Message",
                        INFORMATION_MESSAGE
                );
                orderedItemsList.setText("");
                TotalPrice.setText("Total   " + 0 + "yen ");
                sum = 0;
            }
        });
    }

    int order(String food, int price, int sum) {
        int confirmation = JOptionPane.showConfirmDialog(
                null,
                "Would you like to order " + food + "?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);
        if (confirmation == 0) {
            JOptionPane.showMessageDialog(
                    null,
                    "Order for " + food + " received.",
                    "Message",
                    INFORMATION_MESSAGE
            );
            String currentText = orderedItemsList.getText();
            orderedItemsList.setText(currentText + food + " " + price + " yen\n");
            sum += price;
            TotalPrice.setText("Total   " + sum + "yen ");
        }
        return sum;
    }
}



